#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <iostream>
class Date
{
private:
	int day;
	int month;
	int year;
public:
	Date();
	Date(const Date& d);
	Date(int d, int m, int y);
	~Date();
	int* get_date()const;
	friend istream& operator>>(istream& is, Date& d);
};