#pragma once
using namespace std;
#include <iostream>
#include <string>
#include "Art.h"
#include "Artist.h"
class Museum
{
private:
	int sizeOfArts;
	int sizeOfArtists;
	Art** arts;
	Artist** artists;
public:
	Museum();
	void operator++();
	void addArt(char* name, Date creat, char* str, float h, Artist* art, int siz);
	void operator-(int index);
	void printArtist()const;
	void printArt()const;
	void printImage()const;
	void printStatue()const;
	void printArtOfArtist(char* name)const;
	void printStream(char* stream)const;
	void printArtistOfOneImage()const;
	void printArtistOfOneStatue()const;
	void printStatueOver(float w)const;
	void menu();
	~Museum();
};