#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <iostream>
#include <string>
#include "Artist.h"
#include "Art.h"
#include "Image.h"
Image::Image()
{
	length = 0;
}
Image::Image(char* name, Date creat, char* str, float h, Artist* art, int siz, int l) :Art(name, creat, str, h, art, siz)
{
	length = l;
}
Image::Image(const Image& i) : Art(i)
{
	length = i.length;
}
Image::~Image()
{
}