#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <iostream>
#include <string>
#include "Artist.h"
#include "Art.h"
#include "Museum.h"
#include "Image.h"
#include "Statue.h"
#include"HangingStatue.h"
#define N 20
Museum::Museum()
{
	arts = NULL;
	artists = NULL;
	/*cout << "enter num of artists and num of arts";*/
	sizeOfArtists = 0;
	sizeOfArts=0;
	/*cout << "done:)" << endl;*/
}
void Museum::operator++()//addEmptyArtist
{
	char temp[N];
	char* n;
	Date birthDate,dDeath;
	int size;
	cout << "Please Enter Artist's Name:" << endl;
	cin >> temp;
	n = new char[strlen(temp) + 1];
	if (!n)
		exit(1);
	strcpy(n, temp);
	cout << "Please Enter Artist's BirthDate:" << endl;
	cin >> birthDate;
	cout << "Please Enter Artist's Daeth Date:" << endl;
	cin >> dDeath;
	cout << "Please Enter Artist's Num Of Arts:" << endl;
	cin >> size;
	Artist** artists2 = new Artist* [sizeOfArtists + 1];
	if (!artists2)
	{
		exit(1);
	}
	for (int i = 0; i < sizeOfArtists; i++)
	{
		artists2[i] = artists[i];
	}
	artists2[sizeOfArtists] = artists2[sizeOfArtists]->Empty_Arts_Arr(n, birthDate, dDeath, size);
	cout << "done:)" << endl;
	delete[]artists;
	artists = artists2;
	++this->sizeOfArtists;
}
void Museum::addArt(char* name, Date creat, char* str, float h, Artist* art, int siz)
{

	Art** arts2 = new Art * [sizeOfArts + 1];
	if (!arts2)
	{
		exit(1);
	}
	for (int i = 0; i < sizeOfArts; i++)
	{
		arts2[i] = arts[i];
	}
	arts2[sizeOfArts] = new Art(name, creat, str, h, art, siz);
	if (!arts2[sizeOfArts])
	{
		exit(1);
	}
	delete[]arts;
	arts = arts2;
	++this->sizeOfArts;
}
void Museum::operator-(int index)
{
		
		Art** arts2 = new Art * [sizeOfArts-1];
		if (!arts2)
		{
			exit(1);
		}
		for (int i = 0; i < sizeOfArts - 1; i++)
		{
			if (i == index)
				++i;
			else
			{
				arts2[i] = new Art();
				if (!arts2[i])
					exit(1);
				arts2[i] = arts[i];
			}
		}
		delete[]arts;
		arts = arts2;
		--sizeOfArts;
	
}
void Museum::printArtist()const
{
	for (int i = 0; i < sizeOfArtists; i++)
	{
		if (artists[i]->get_size() > 0)
		{
			cout << *artists[i] << endl;
		}
	}
}
void Museum::printArt()const
{
	for (int i = 0; i < sizeOfArts; i++)
	{
		cout << *arts[i] << endl;
	}
}
void Museum::printImage()const
{
	for (int i = 0; i < sizeOfArts; i++)
	{
		if (typeid(arts[i]) == typeid(Image))
			cout << *arts[i] << endl;
	}
}
void Museum::printStatue()const
{
	for (int i = 0; i < sizeOfArts; i++)
	{
		if (typeid(arts[i]) == typeid(Statue) || typeid(arts[i]) == typeid(HangingStatue))
			cout << *arts[i] << endl;
	}
}
void Museum::printArtOfArtist(char* name)const
{
	for (int i = 0; i < sizeOfArtists; i++)
	{
		if (strcmp(artists[i]->get_name(), name) == 0)
		{
			artists[i]->print_art();
		}
	}
}
void Museum::printStream(char* stream)const
{
	for (int i = 0; i < sizeOfArts; i++)
	{
		if (strcmp(arts[i]->get_stream(), stream) == 0)
			cout << *artists[i] << endl;
	}
}
void Museum::printArtistOfOneImage()const
{
	for (int i = 0; i < sizeOfArtists; i++)
	{
		if (artists[i]->numImage() > 0)
			cout << *artists[i] << endl;
	}
}
void Museum::printArtistOfOneStatue()const
{
	for (int i = 0; i < sizeOfArtists; i++)
	{
		if (artists[i]->numStatue() > 0)
			cout << *artists[i] << endl;
	}
}
void Museum::printStatueOver(float w)const
{
	for (int i = 0; i < sizeOfArts; i++)
	{
		Statue* s = dynamic_cast<Statue*>(arts[i]);
		if (!s)
			exit(1);
		else
			if ((*s).get_weight() > w)
				cout << *arts[i] << endl;
	}
}
Museum::~Museum()
{
	delete[]arts;
	delete[]artists;
}
void Museum::menu()
{

	int choice, siz;
	char* name, * str, word[N], sword[N];
	Date creat;
	float h, weight;
	char wnword[N];
	Artist* artist = new Artist();
	artist = artist->init_Artist();
	char* wname;
	cout << "~Menu~" << endl;
	cout << "2� Add an artist with an empty array of works" << endl;
	cout << "3� Add a creation" << endl;
	cout << "4� Remove a creation" << endl;
	cout << "5� Print of all artists who have at least one piece	" << endl;
	cout << "6� Print all works" << endl;
	cout << "7� Print all images" << endl;
	cout << "8� Printing all sculptures" << endl;
	cout << "9� Print all the works of a particular artist" << endl;
	cout << "10� Print all the works from a particular stream" << endl;
	cout << "11� Print all artists who have at least one picture" << endl;
	cout << "12� Printing all artists who have at least one statue" << endl;
	cout << "13� Printing all sculptures above a certain weight" << endl;
	cin >> choice;
	do
	{
		switch (choice)
		{
		case 1:
			/*Museum();*/
			break;
		case 2:
			operator++();
			break;
		case 3:
			cout << "Please Enter Artist's Name:" << endl;
			cin >> wnword;
			wname = new char[strlen(wnword) + 1];
			if (!wname)
				exit(1);
			strcpy(wname, wnword);
			for (int i = 0; i < sizeOfArtists; ++i)
			{
				if (strcmp(artist[i].get_name(), wname) == 1)//if is not equivalent
					cout << "Artist Doesn't Have This Art." << endl;
			}

			cout << "Please Enter Name And Stream:" << endl;
			cin >> word;
			cin >> sword;
			name = new char[strlen(word) + 1];
			if (!name)
				exit(1);
			strcpy(name, word);
			str = new char[strlen(sword) + 1];
			if (!str)
				exit(1);
			strcpy(str, sword);
			strcpy(name, word);
			cout << "Please Enter Creation Date:" << endl;
			cin >> creat;
			cout << "Please Enter Artist's Passaway Date,else if Alive Insert 0.0.0" << endl;
			{
				Date death;
				cin >> death;
			}
			cout << "Enter Height:" << endl;
			cin >> h;
			cout << "Please Enter Amount Of Arts:" << endl;
			cin >> siz;
			{
				Art** arr = new Art * [siz];
				if (!arr)
					exit(1);
				for (int i = 0; i < siz; ++i)
				{
					/*artist = artist[i].Empty_Arts_Arr();*/
					arr[i] = new Art(wname, creat, str, h, artist, siz);
					if (!arr[i])
						exit(1);
				}
			}
			addArt(wname, creat, str, h, artist, siz);// size updated in function.
			/*cout << "done:)" << endl;*/
			break;
		case 4:
			int index;
			cout << "Please Enter Index Of Art To Remove:" << endl;
			cin >> index;
			operator-(index);
			cout << "done:)" << endl;
			break;
		case 5:
			printArtist();
			break;
		case 6:
			printArtist();
		case 7:
			printImage();
			break;
		case 8:
			printStatue();
			break;
		case 9:
			cout << "Please Enter The Name Of Desired Artist: " << endl;
			cin >> word;
			name = new char[strlen(word) + 1];
			if (!name)
				exit(1);
			strcpy(name, word);
			printArtOfArtist(name);
			break;
		case 10:
			cout << "Please Enter Stream:" << endl;
			str = new char[strlen(sword) + 1];
			if (!str)
				exit(1);
			cin >> str;
			strcpy(str, sword);
			printStream(str);
			break;
		case 11:
			printArtistOfOneImage();
			break;
		case 12:
			printArtistOfOneStatue();
			break;
		case 13:
			cout << "Please Enter Statue's Weight:" << endl;
			cin >> weight;
			printStatueOver(weight);
			break;
		default:
			cout << "Invaild Input,Try Again." << endl;
		}

	} while (choice > 1 && choice < 14);


}