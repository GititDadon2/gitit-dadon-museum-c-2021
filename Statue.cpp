#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <iostream>
#include <string>
#include "Artist.h"
#include "Art.h"
#include "Statue.h"
Statue::Statue()
{
	area = 0.0;
	weight = 0.0;
}
Statue::Statue(char* name, Date creat, char* str, float h, Artist* art, int siz, float a, float w) :Art(name, creat, str, h, art, siz)
{
	area = a;
	weight = w;
}
Statue::Statue(const Statue& s) : Art(s)
{
	area = s.area;
	weight = s.weight;
}
Statue::~Statue()
{
}