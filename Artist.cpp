#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <string>
#include <iostream>
#include "artist.h"
#include "Image.h"
#include "Statue.h"
#define N 20
Artist::Artist()
{
	size = 0;
	name = NULL;
	birthdate = Date::Date();
	death = Date::Date();
	pics = 0;
}
Artist::Artist(const Artist& s)
{
	name = new char[strlen(s.name) + 1];
	if (!name)
		exit(1);
	strcpy(name, s.name);
	birthdate = Date::Date(s.birthdate);
	death = Date::Date(s.death);
	for (int i = 0; i < s.size; ++i)
	{
		pics[i] = s.pics[i];
	}
}
Artist::Artist(char* n, Date bdate, Date dth, Art** pic, int siz)
{
	size = siz;
	name = new char[strlen(n) + 1];
	if (!name)
	{
		exit(1);
	}
	strcpy(name, n);
	birthdate = Date::Date(bdate);
	death = Date::Date(dth);
	pics = new Art * [siz];
	if (!pics)
		exit(1);
	for (int i = 0; i < siz; ++i)
	{
		//pics[i] = new Art(); how to initialize art??
		pics[i] = pic[i];
	}
}
ostream& operator<<(ostream& os, const Artist& s)
{

	os << "Artist's Name:" << endl << s.name << endl << "BirthDate:" << s.birthdate.get_date() << endl;
	os << "Date Of Death:" << endl << s.death.get_date() << endl;
	os << "Artist's Arts Are:" << endl;
	for (int i = 0; i < s.size; ++i)
	{
		cout << (*s.pics[i]) << endl;
	}
	return os;
}
Artist Artist::operator+(const Art& pic)
{
	pics[size - 1] = new Art(pic);
	if (!pics[size - 1])
		exit(1);
	Art** new_ar = new Art * [size + 1];
	for (int i = 0; i < size; ++i)
	{
		new_ar[i] = pics[i];
	}
	delete[]pics;
	pics = new_ar;
	++this->size;
	return(*this);
}
void Artist::print_art()const
{
	for (int i = 0; i < size; ++i)
	{
		cout << (*pics[i]) << endl;
	}
}
Artist::~Artist()
{

}
int Artist::numImage()
{
	int n = 0;
	for (int i = 0; i < size; i++)
	{
		if (typeid(pics[i]) == typeid(Image))
			++n;
	}
	return n;
}
int Artist::numStatue()
{
	int n = 0;
	for (int i = 0; i < size; i++)
	{
		if (typeid(pics[i]) == typeid(Statue))
			++n;
	}
	return n;
}
Artist* Artist::Empty_Arts_Arr(char* n,Date birthDate,Date dDeath,int size)
{
	Artist* new_artist;
	Art** new_arr = new Art * [size];
	for (int i = 0; i < size; ++i)
	{
			new_arr[i] = new Art();
	}
	new_artist = new Artist(n, birthDate, dDeath, new_arr, size);
	return new_artist;
}
Artist* Artist::init_Artist()
{
	Artist* artist;
	char* nam, word[N];
	Date birthdat;
	Date deat;
	Art** pic;
	int siz, num;
	cout << "Enter Number Of Artists:" << endl;
	cin >> num;
	cout << "Please Enter Artist's Name:" << endl;
	nam = new char[strlen(word) + 1];
	if (!nam)
		exit(1);
	cin >> word;
	strcpy(nam, word);
	cout << "Please Enter Artist's BrithDate:" << endl;
	cin >> birthdat;
	cout << "Please Enter Artist's Death Date,If Alive Insert 0.0.0" << endl;
	cin >> deat;
	cout << "Please Enter Number Of Artist's Arts:" << endl;
	cin >> siz;
	pic = new Art * [siz];
	if (!pic)
		exit(1);
	for (int i = 0; i < siz; ++i)
	{
		pic[i] = new Art();//empty array
	}
	artist = new Artist(nam, birthdat, deat, pic, siz);
	if (!artist)
		exit(1);
	return artist;
}