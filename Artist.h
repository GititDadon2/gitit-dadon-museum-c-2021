#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <iostream>
#include "Art.h"
class Artist
{
private:
	char* name;
	Date birthdate;
	Date death;
	Art** pics;
	int size;

public:
	Artist();
	Artist(const Artist& s);
	Artist(char* n, Date bdate, Date dth, Art** pic, int siz);
	friend ostream& operator<<(ostream& os, const Artist& s);
	Artist operator+(const Art& pic);
	~Artist();
	int get_size()const { return size; }
	char* get_name()const { return name; }
	void print_art()const;
	int numImage();
	int numStatue();
	Artist* init_Artist();
	Artist* Empty_Arts_Arr(char* n, Date birthDate, Date dDeath, int size);
};