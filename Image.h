#pragma once
using namespace std;
#include <iostream>
#include <string>
#include"Art.h"
class Image :virtual public Art
{
private:
	int length;
public:
	Image();
	Image(char* name, Date creat, char* str, float h, Artist* art, int siz, int l);
	Image(const Image& i);
	~Image();
};