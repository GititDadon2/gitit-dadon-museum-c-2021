#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <string>
#include <iostream>
#include "Art.h"
Art::Art()
{
	name = NULL;
	creation = Date::Date();
	stream = NULL;
	height = 0.0;
	artist = NULL;
}
Art::Art(const Art& a)
{
	name = new char[strlen(a.name) + 1];
	if (!name)
		exit(1);
	creation = Date::Date(a.creation);
	stream = new char[strlen(a.stream) + 1];
	if (!stream)
		exit(1);
	height = a.height;
	artist = a.artist;
}
Art::Art(char* n, Date create, char* flow, float he, Artist* artis, int siz)
{
	size = siz;
	name = new char[strlen(n) + 1];
	if (!name)
		exit(1);
	creation = Date::Date(create);
	stream = new char[strlen(flow) + 1];
	if (!stream)
		exit(1);
	height = he;
	artist = artis;//check
}


Art::~Art()
{
	delete[]stream;
	delete[]name;
}
ostream& operator<<(ostream& os, const Art& a)
{
	os << "Name" << endl << a.name << "Stream:" << endl << a.stream << "Creation Date:" << endl << a.creation.get_date() << endl;
	os << "Height:" << endl << a.height << endl;
	os << a.artist;
	return os;
}