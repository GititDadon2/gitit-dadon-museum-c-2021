#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <string>
#include <iostream>
#include "Date.h"
Date::Date()
{
	day = 0;
	month = 0;
	year = 0;
}
Date::Date(const Date& d)
{
	day = d.day;
	month = d.month;
	year = d.year;
}
Date::Date(int d, int m, int y)
{
	day = d;
	month = m;
	year = y;
}
int* Date::get_date()const
{
	int* arr = new int[3];
	if (!arr)
		exit(1);
	arr[0] = day;
	arr[1] = month;
	arr[2] = year;
	return arr;

}
Date::~Date()
{
}
istream& operator>>(istream& is, Date& d)
{
	is >> d.day >> d.month >> d.year;
	return is;
}