#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <iostream>
#include "Date.h"
class Artist;class Art
{
protected:
	char* name;
	Date creation;
	char* stream;
	float height;
	Artist* artist;
	int size;
public:
	Art();
	Art(const Art& a);
	Art(char* n, Date create, char* flow, float he, Artist* artis, int siz);
	char* get_stream()const { return stream; }
	virtual ~Art();
	/*void print_arts()const;*/

	int get_sizeArt()const { return size; }
	friend ostream& operator<<(ostream& os, const Art& a);
};