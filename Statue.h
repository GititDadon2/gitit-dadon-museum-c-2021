#pragma once
using namespace std;
#include <iostream>
#include <string>
#include"Art.h"
class Statue :virtual public Art
{
private:
	float area, weight;
public:
	Statue();
	Statue(char* name, Date creat, char* str, float h, Artist* art, int siz, float a, float w);
	Statue(const Statue& s);
	~Statue();
	float get_weight()const { return weight; }
};