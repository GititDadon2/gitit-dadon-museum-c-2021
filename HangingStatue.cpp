#define _CRT_SECURE_NO_WARNINGS
using namespace std;
#include <iostream>
#include <string>
#include "Artist.h"
#include "Art.h"
#include "HangingStatue.h"
HangingStatue::HangingStatue() :Art(), Image(), Statue()
{
	screws = 0;
}
HangingStatue::HangingStatue(char* name, Date creat, char* str, float h, Artist* art, int siz, float a, float w, int l, int s) : Art(name, creat, str, h, art, siz), Image(name, creat, str, h, art, siz, l), Statue(name, creat, str, h, art, siz, a, w)
{
	screws = s;
}
HangingStatue::HangingStatue(const HangingStatue& hs) : Art(hs), Image(hs), Statue(hs)
{
	screws = hs.screws;
}
HangingStatue::~HangingStatue()
{
}