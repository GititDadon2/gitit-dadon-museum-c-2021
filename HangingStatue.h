#pragma once
using namespace std;
#include <iostream>
#include <string>
#include "Image.h"
#include "Statue.h"
class HangingStatue :public Image, public Statue
{
private:
	int screws;
public:
	HangingStatue();
	HangingStatue(char* name, Date creat, char* str, float h, Artist* art, int siz, float a, float w, int l, int s);
	HangingStatue(const HangingStatue& hs);
	~HangingStatue();
};